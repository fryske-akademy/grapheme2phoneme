package org.fryske_akademy.graph2phon;

/*-
 * #%L
 * pos-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

public class RconnectionFactory extends BasePooledObjectFactory<WrappedRConnection> {

    public static final String INPUT_R_VAR_NAME = "text";
    public static final String INPUT_R_POS_NAME = "pos";
    public static final String INPUT_R_LEMMA_NAME = "lemma";

    private final String loadScript, modelPath, udModelPath;

    public RconnectionFactory(String modelPath, String loadScript, String udModelPath) {
        this.loadScript = loadScript;
        this.modelPath = modelPath;
        this.udModelPath = udModelPath;
    }

    @Override
    public WrappedRConnection create() throws Exception {
        return new WrappedRConnection(modelPath,loadScript,udModelPath);
    }

    @Override
    public PooledObject<WrappedRConnection> wrap(WrappedRConnection rConnection) {
        return new DefaultPooledObject<>(rConnection);
    }

    @Override
    public boolean validateObject(PooledObject<WrappedRConnection> p) {
        return p.getObject().isConnected();
    }

    @Override
    public void destroyObject(PooledObject<WrappedRConnection> p) throws Exception {
        p.getObject().deleteOutput();
        p.getObject().close();
    }

    @Override
    public void passivateObject(PooledObject<WrappedRConnection> p) throws Exception {
        p.getObject().deleteOutput();
        p.getObject().assign(INPUT_R_VAR_NAME,"");
        p.getObject().assign(INPUT_R_POS_NAME,"");
        p.getObject().assign(INPUT_R_LEMMA_NAME,"");
    }


}
