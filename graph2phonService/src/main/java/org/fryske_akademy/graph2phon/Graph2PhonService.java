
package org.fryske_akademy.graph2phon;

/*-
 * #%L
 * pos-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.PathParam;
import org.fa.tei.jaxb.facustomization.Join;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPNull;
import org.rosuda.REngine.REXPString;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.servlet.ServletContext;
import jakarta.validation.constraints.Size;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Properties;
import java.util.UUID;

@Stateless
@Path(Graph2PhonService.ROOT)
public class Graph2PhonService {

    public static final String ROOT = "graph2phon";
    public static final String INFOPATH = "info";
    private static final Logger LOGGER = LoggerFactory.getLogger(Graph2PhonService.class);
    public static final String SCRIPTDIR = "/resources";
    private static final String SERVICE_R = "service.R";
    private static final int MAX = 2048000;

    @Context
    private ServletContext servletContext;

    @Inject
    private RConnectionPool rConnectionPool;

    private final REXPNull rnull = new REXPNull();

    private File toPhonemes(String text, String lemma, Join.Pos pos) throws Exception {
        String scriptDir = servletContext.getRealPath(SCRIPTDIR);
        WrappedRConnection rConnection = rConnectionPool.borrow();
        try {
            File tempFile = new File(UUID.randomUUID().toString());
            File phonemes = rConnection.initOutput();
            try {
                rConnection.assign(RconnectionFactory.INPUT_R_VAR_NAME, text);
                if (lemma != null)
                    rConnection.assign(RconnectionFactory.INPUT_R_LEMMA_NAME, lemma);
                else
                    rConnection.assign(RconnectionFactory.INPUT_R_LEMMA_NAME, rnull);
                if (pos != null)
                    rConnection.assign(RconnectionFactory.INPUT_R_POS_NAME, pos.name().toUpperCase());
                else
                    rConnection.assign(RconnectionFactory.INPUT_R_POS_NAME, rnull);
                rConnection.eval("source(\"" + scriptDir + File.separator + SERVICE_R + "\")");
//                rConnection.assign(".tmp.", "source(\"" + scriptDir + File.separator + SERVICE_R + "\")");
//                REXP r = rConnection.parseAndEval("try(eval(parse(text=.tmp.)),silent=TRUE)");
//                if (r.inherits("try-error")) {
//                    System.err.println(((REXPString)r).toDebugString());
//                }
                Files.move(phonemes.toPath(), tempFile.toPath());
                return tempFile;
            } catch (RserveException | IOException ex) {
                tempFile.delete();
                ex.printStackTrace();
                throw ex;
            }
        } finally {
            rConnectionPool.putBack(rConnection);
        }
    }

    @GET
    @Path(INFOPATH)
    @Produces({MediaType.APPLICATION_JSON})
    public Response info() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("version", versionInfo())
                .add("maxTextSize", MAX)
                .add(INFOPATH, "/" + RestApplication.ROOT + "/" + ROOT + "/" + INFOPATH)
                .build();
        return Response.ok(jsonObject).build();
    }

    @POST
    @Path("process")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces("text/tab-separated-values")
    public Response tagTsv(@Size(min = 1, max = MAX) String text) {
        return getResponse(text, null, null);
    }

    @GET
    @Path("tagWord/{word}/{lemma}/{pos}")
    @Produces("text/tab-separated-values")
    public Response tagWord(@NotNull @NotEmpty @PathParam("word") String word,
                            @DefaultValue("X") @PathParam("lemma") String lemma,
                            @DefaultValue("x") @PathParam("pos") Join.Pos pos
    ) {
        return getResponse(word, lemma, pos);
    }

    private Response getResponse(String text, String lemma, Join.Pos pos) {
        try {
            final StreamingOutput output = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    try {
                        File tempFile = toPhonemes(text, lemma, pos);
                        try {
                            Files.copy(tempFile.toPath(), output);
                        } finally {
                            tempFile.delete();
                        }
                    } catch (Exception ex) {
                        LOGGER.warn("unable to process", ex);
                        throw new WebApplicationException(
                                Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                                        deepestCause(ex).getMessage()).build());
                    }
                }
            };
            return Response.ok(output).build();
        } catch (WebApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                            deepestCause(ex).getMessage()).build());
        }
    }

    public static Throwable deepestCause(Throwable t) {
        if (t.getCause() != null) {
            return deepestCause(t.getCause());
        }
        return t;
    }

    private static final Properties BUILDPROPERTIES = new Properties();

    static {
        try {
            BUILDPROPERTIES.load(Graph2PhonService.class.getResourceAsStream("/build.properties"));
        } catch (IOException e) {
            LOGGER.warn("unable to load build properties", e);
        }
    }

    public String versionInfo() {
        return BUILDPROPERTIES.toString();
    }

}
