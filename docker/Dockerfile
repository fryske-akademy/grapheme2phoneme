# syntax=docker/dockerfile:experimental
FROM payara/server-web:6.2024.4-jdk17

ENV DEBIAN_FRONTEND noninteractive

USER root

RUN apt update && apt -y upgrade && apt -y install software-properties-common && \
apt -y install dirmngr wget libssl-dev libcurl4-openssl-dev libxml2-dev libpoppler-cpp-dev && \
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | apt-key add - && \
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" && \
apt -y install r-base

RUN apt -y install python3-pip && \
python3 -m pip install --upgrade pip && \
pip3 install phonetisaurus

COPY install.R ./

RUN Rscript ./install.R

USER payara

ARG VERSION

WORKDIR /opt/payarasetup

COPY graph2phon-service-$VERSION.war ./
COPY deploy.txt ./

RUN --mount=type=secret,id=pwdprd,required,mode=0444 \
--mount=type=secret,id=changepwd,required,mode=0444 \
${PAYARA_DIR}/bin/asadmin --user admin --passwordfile=/run/secrets/changepwd change-master-password && \
${PAYARA_DIR}/bin/asadmin --user admin --passwordfile=/run/secrets/changepwd change-admin-password && \
${PAYARA_DIR}/bin/asadmin -u admin -W /run/secrets/pwdprd start-domain && \
${PAYARA_DIR}/bin/asadmin -u admin -W /run/secrets/pwdprd multimode --file deploy.txt
