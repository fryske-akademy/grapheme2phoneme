#!/usr/bin/Rscript --vanilla

################################################################################
#                                                                              #
# Script for converting orthographic transcriptions into phonetic IPA          #
# transcriptions using Phonetisaurus.                                          #
#                                                                              #
# Before running the script install Phonetisaurus. For this purpose, Python    #
# 3.7 or newer must already be installed. Enter the following command in a     #
# terminal:                                                                    #
#                                                                              #
# $ sudo pip3 install phonetisaurus                                            #
# $ ln -s /usr/bin/python3.7 /usr/bin/python3                                  #
#                                                                              #
# Addionally, install the libraries libcurl, libxml-2.0 and poppler-cpp. When  #
# using Ubuntu 20.04 or later, enter the following commands in a terminal:     #
#                                                                              #
# $ sudo apt install r-base-core                                               #
# $ sudo apt install libcurl4-openssl-dev                                      #
# $ sudo apt install libxml2                                                   #
# $ sudo apt install libxml2-dev                                               #
# $ sudo apt install libpoppler-cpp-dev                                        #
# $ sudo apt install libssl-dev                                                #
#                                                                              #
# When running the script for the first time, missing R packages are installed #
# which can take a while!                                                      #
#                                                                              #
# The file 'g2p.fst' should reside in the same directory as where this script  #
# is stored. This file contains the trained g2p corpus. The corpus was trained #
# on the basis of the Frysk Hânwurdboek, the Foarkarswurdlist and a short      #
# supplementary list.                                                          #
#                                                                              #
# The file 'Frysk.txt' is added as an example text. It can be processed by     #
# entering the following command in a terminal:                                #
#                                                                              #
# $ ./graph2phon.R -i Frysk.txt -f -o Frysk.xlsx -x                            #
#                                                                              #
# where the result is stored as an Excel spreadsheet in Frysk.xlsx             #
#                                                                              #
# or read from pipe:                                                           #
#                                                                              #
# $ cat Frysk.txt|./graph2phon.R -i - -e -t > Frysk.tsv                        #
#                                                                              #
# or read from user input (close with Ctrl-d):                                 #
#                                                                              #
# $ ./graph2phon.R -i - -e -t > Frysk.tsv                                      #
#                                                                              #
# where the result is stored as a tab-separated file in Frysk.tsv.             #
#                                                                              #
# For information about usage and options enter:                               #
#                                                                              #
# $ ./graph2phon.R -h                                                          #
#                                                                              #
# Copyright: Fryske Akademy, Leeuwarden, The Netherlands, 24 February 2021.    #
# Contact  : wheeringa@fryske-akademy.nl                                       #
#                                                                              #
################################################################################

# install and load packages

packages = c("optparse", "readr", "readtext", "xml2", "rvest", "openxlsx", "stringr", "udpipe", "ipa")

for (p in packages)
{
  if (suppressWarnings((!library(p, character.only=T, logical.return = T, quietly = T))))
  {
    cat("\nInstalling package", p, "...\n", file = stderr())
    suppressWarnings(install.packages(p, quiet = T))
  }
  
  if (suppressWarnings((!library(p, character.only=T, logical.return = T, quietly = T))))
  {
    cat("\nPackage", p, "not installed!\n\n", file = stderr())
    quit(status=1)
  }
}

usage = "usage: %prog -i INPUT -e|-f|-w -l LEMMA -p POSTAG -o OUTPUT -t|-x|-c"

option_list = list(
  make_option(c("-i", "--input" ), action="store"     , help="text or file name or url of website"),

  make_option(c("-e", "--text"  ), action="store_true", help="input  is some text between ' and '"),
  make_option(c("-f", "--file"  ), action="store_true", help="input  is file (.txt, .docx, .html)"),
  make_option(c("-w", "--web"   ), action="store_true", help="input  is URL of website"),

  make_option(c("-l", "--lemma" ), action="store"     , help="UD lemma"),
  make_option(c("-p", "--postag"), action="store"     , help="UD Part-of-speech tag"),

  make_option(c("-o", "--output"), action="store"     , help="name of output file"),
  
  make_option(c("-t", "--tsv"   ), action="store_true", help="output is tab-separated file" ),
  make_option(c("-x", "--xlsx"  ), action="store_true", help="output is Microsoft Excel file")
); 

option_parser <- OptionParser(usage=usage, option_list=option_list)
opt <- parse_args(option_parser)

if (length(opt) == 1)
{  
  print_help(option_parser)
  quit(status=1)
}

if (is.null(opt$input))
{
  con <- file("stdin")
  string <- scan(con, what=character(), quote="")
  close(con)
}                    else
if (unlist(opt$input) == "-")
{
  con <- file("stdin")
  string <- scan(con, what=character(), quote="")
  close(con)
}                    else
{
  string <- unlist(opt$input)
}

if ((!is.null(opt$text) && (opt$text==T)) & ( is.null(opt$file)) & ( is.null(opt$web)))
  input <- "text"    else

if (( is.null(opt$text)) & (!is.null(opt$file) && (opt$file==T)) & ( is.null(opt$web)))
  input <- "file"    else

if (( is.null(opt$text)) & ( is.null(opt$file)) & (!is.null(opt$web) && (opt$web==T)))
  input <- "web"     else
{  
  print_help(option_parser)
  quit(status=1)
}

if (is.null(opt$output))
{
  result <- stdout()
}                    else
if (unlist(opt$output) == "-")
{
  result <- stdout()
}                    else
{
  result <- unlist(opt$output)
}

if ((!is.null(opt$tsv) && (opt$tsv==T)) & (is.null(opt$xlsx)) & (is.null(opt$connlu)))
  output <- "tsv"    else

if ((is.null(opt$tsv)) & (!is.null(opt$xlsx) && (opt$xlsx==T)) & (is.null(opt$connlu)))
  output <- "xlsx"   else
{  
  print_help(option_parser)
  quit(status=1)
}  

# read data

if (input=="text")
{
  s <- string
}
  
if (input=="file")
{
  if (file.exists(string))
  {
    s <- readtext(file = string, encoding = "UTF-8")
  }
  else 
  {
    cat("File ", string, "not found.\n", file = stderr())
    quit(status=1)
  }
}

if (input=="web")
{
  site <- NULL
  
  tryCatch(
    site <- read_html(string),
    error   = function(something) {},
    warning = function(something) {}
  )
  
  if (length(site)>0)
  {
    text <- html_text(html_nodes(site, 'p'))
    text <- gsub("\n", "", text)
    text <- gsub("([)[0-9]+(]))", "", text)
    
    text <- data.frame(text)
    text <- subset(text, str_count(text, "\\w+") > 1)
    text <- subset(text, grepl("[A-Z|a-z]", text))
    
    if (nrow(text) > 0)
      s <- paste(text$text, sep = "", collapse = "\n\n")
    else
      s <- ""
  }
  else
  {
    cat("\nNo website found at given URL!\n\n", file = stderr())
    quit(status=1)
  }
}

s <- paste(s, collapse = " ")

if (trimws(s) == "")
  quit(status=1)

# process text

checkWithF <- function(gi, pi, lemma, pos)
{
  if (grepl("ôf$", gi))
  {
    if (!grepl("chôf$", gi) & grepl("hôf$", gi))
      withF <- T
    else
    
    if (grepl("(^|d|l|k|t|m|n)(r|d|l)ôf", gi))
      withF <- T
    else
      
    if (grepl("((ke)|(me)|(ne)|(ea)|(ee))(r|d)ôf", gi))
      withF <- T
    else
      withF <- F
      
    if (withF & !grepl("f$", pi))
      pi <- gsub("ɔː$", "ɔːf", pi)
    else
        
    if (!withF & grepl("f$", pi))
      pi <- gsub("ɔːf$", "ɔː", pi)
    else {}
  }
  else
    
  if  ((toupper(pos)=="VERB") & grepl( "ytse$", lemma) & grepl("ytst$" , gi))
    pi <- gsub("ist$", "itst", pi)
  else
      
  if  ((toupper(pos)=="VERB") & grepl("oetse$", lemma) & grepl("oetst$", gi))
    pi <- gsub("ust$", "utst", pi)
  else
        
  if  ((toupper(pos)=="VERB") & grepl( "etse$", lemma) & grepl("etst$" , gi))
    pi <- gsub("ɛst$", "ɛtst", pi)
  else
  
  if  ((toupper(pos)=="VERB") & grepl("ɛst$", pi))
    pi <- gsub("ɛst$", "əst", pi)
  else
  
  if  ((toupper(pos)=="VERB") & grepl( "ɛt$", pi))
    pi <- gsub( "ɛt$",  "ət", pi)
  else
    
  if (((toupper(pos)=="NOUN") | (toupper(pos)=="ADJ")) & (gi!=lemma))
  {
    if (grepl("ɛrs$", pi))
      pi <- gsub("ɛrs$" ,  "ərs", pi)
    else
        
    if (grepl( "ɛs$", pi))
      pi <- gsub( "ɛs$" ,   "əs", pi)
    else {}
        
    if (grepl("zers$", gi) & grepl( "zəs$", pi))
      pi <- gsub("zəs$" ,  "zrs", pi)
    else
          
    if (grepl("gers$", gi) & grepl( "ɡɛːs$", pi))
      pi <- gsub("ɡɛːs$",  "ɡrs", pi)
    else {}  
  }
  else
  
  if (grepl("ysk$", gi) & grepl("is$", pi))
    pi <- gsub("is$", "isk", pi)
  else {}

  return(pi)
}

graph2phon <- function(ud)
{
  p <- gsub("'" , "\\'", ud$token, fixed = TRUE)
  p <- gsub("^-", "", p)
  p <- gsub("[0-9]+", "digits", p)
  p <- tolower(paste(p, collapse = " "))
  p <- unlist(system(command = paste0("phonetisaurus predict --model g2p.fst --casing ignore ", p), intern = TRUE))
 
  if (length(p) == nrow(ud))
  {
    df <- data.frame()
    
    for (i in 1:length(p))
    {
      sep <- str_locate(p[i], " ")[1]
      
      gi <- substr(p[i], 1, sep-1)
      
      pi <- substr(p[i], sep+1, nchar(p[i]))
      pi <- gsub(" ", "", pi)
      pi <- gsub("tt$", "t", pi)
      pi <- checkWithF(gi, pi, ud$lemma[i], ud$upos[i])
      
      df <- rbind(df, data.frame(graphemic=gi, phonemic=pi))
    }
    
    df$xsampa    <- ipa(df$phonemic, to="xsampa")
    
    df$graphemic <- ud$token
    df$lemma     <- ud$lemma
    df$upos      <- ud$upos
    
    df$phonemic  <- gsub("diɡits", "...", df$phonemic )
    df$xsampa    <- gsub("digits", "...", df$xsampa   )
    
    return(df)
  }
  else
  {
    cat("\nThe text likely contains one or more words composed exclusively of unknown tokens!\n\n", file = stderr())
    quit(status=1)
  }
}

annotateUD <- function(s)
{
  s <- str_replace_all(s, "[^[:alnum:][:space:]’]", "")
  s <- str_replace_all(s, "^\\’(?=([:alpha:][:alpha:]))", "")
  s <- str_replace_all(s, "(?<=([:space:]|[:punct:]))\\’(?=([:alpha:][:alpha:]))", " ")
  s <- str_replace_all(s, "\\’ ", " ")
  s <- str_replace_all(s, "\\’$", "")

  return(as.data.frame(udpipe(x = s, object = udpipe_load_model(file = "fy_frysk-ud-1.0-20231124.udpipe"))))
}

processText <- function(s, optLemma, optPosTag)
{
  if ((is.null(optLemma)) & (is.null(optPosTag)))
    result <- annotateUD(s)[,9:11]
  else
  
  if (!grepl(" ", s))
  {
	if ((!is.null(optLemma)) & ( is.null(optPosTag)))
	{
      result <- annotateUD(s)[,9:11]
      result$lemma <- optLemma
	}
	else
	
	if (( is.null(optLemma)) & (!is.null(optPosTag)))
	{
      result <- annotateUD(s)[,9:11]
      result$upos  <- optPosTag
	}
	else
	
	if ((!is.null(optLemma)) & (!is.null(optPosTag)))
    {
	  result <- data.frame(
	    token = s,
	    lemma = optLemma,
	    upos  = optPosTag
	  )
    }	
	else {}
  }
  else
  {
    cat("\nLemma and/or POS tag cannot be combined with multiple words!\n\n", file = stderr())
    quit(status=1)
  }  

  return(result)
}

# write result

if  (output=="tsv")
  write.table(graph2phon(processText(s, opt$lemma, opt$postag)), result, sep = "\t", na = "", dec = ".", row.names = FALSE, col.names = TRUE)

if ((output=="xlsx") & (result!=stdout()))
  write.xlsx (graph2phon(processText(s, opt$lemma, opt$postag)), result, sheetName = "table", headerStyle = createStyle(textDecoration = "BOLD"), rowNames=FALSE, colNames=TRUE, na.string = "", firstRow = TRUE)

if ((output=="xlsx") & (result==stdout()))
  cat("\nMicrosoft Excel file cannot be printed to stdout!\n", file = stderr())

cat("\nDONE\n\n", file = stderr())
