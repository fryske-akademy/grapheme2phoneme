# Frisian grapheme to phoneme in R #

## Installation

**The service (when running outside docker) requires installation of the linux package mentioned in [README.md](README.md) and installation of R packages via [install.R](docker/install.R).**

## Docker setup
Based on jax-rs service running in payara web profile

### create file 'changepwd' to change master and admin password:
```
AS_ADMIN_PASSWORD=admin
AS_ADMIN_NEWPASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=changeit
AS_ADMIN_NEWMASTERPASSWORD=xxxxxxx
```
### create file 'pwdprd' to use new passwords:
```
AS_ADMIN_PASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=xxxxxxx
```
### DON'T COMMIT THE PASSWORD FILES AND REMOVE AFTER USE!!!

### build the application

export VERSION=x.x
clone this repo
git checkout tags/graph2phon-service-$VERSION
mvn clean verify
cp target/graph2phon-service-$VERSION.war docker

```
cd docker
# adapt deploy.txt as needed
export DOCKER_BUILDKIT=1
export APPNAME=graph2phon-ws
docker build --build-arg VERSION=$VERSION --secret id=changepwd,src=changepwd --secret id=pwdprd,src=pwdprd -t $APPNAME:$VERSION .
```

### Run the application
NOTE: connections are pooled, by default the pool size is 3. Memory consumed by input, output may exceed available memory.

```
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
once: echo "AS_ADMIN_MASTERPASSWORD=xxxxxxx"|docker secret create master -
once: adapt graph2phon.properties for your situation
export properties=<path to graph2phon.properties>
export model=<path to g2p.fst>
export udmodel=<path to udpipe model>
docker stack deploy -c docker-compose.yml $APPNAME:$VERSION
```

### Who do I talk to? ###

* edrenth fryske-akademy nl
